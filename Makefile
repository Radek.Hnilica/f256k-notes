# ! make # -*- fill-column: 79; -*-
# $Id: Makefile,v 0.3 2024-01-04 00:13:13+01 radek Exp radek $
# $Source: /home/radek/st/repo/f256k/src/notes/Makefile,v $
# Makefile for F256K notes
#
# Copyright (c) 2023 Radek Hnilica
# License: GPLv3, or ask me for another one.

# Get the system os we are running on
SYSTEM=$(shell uname -s)

### C compiler #########################################################
# On MacBook I'm using different compiler and need to define it there.
#
# NOTE: I do not do build on MacOS for long time.  So this is just legacy, not
# actually used & tested.
ifeq ($(SYSTEM),Darwin)
CC      = gcc-12
endif

STD	:= -std=c11 -g
STACK	:=
WARNS	:= -Wall -Wextra -pedantic -Wconversion
CFLAGS	:= $(STD) $(STACK) $(WARNS)

# Ad some timestamping macros with actual information
CFLAGS+=-Wno-builtin-macro-redefined \
-D__BUILDTIME__=$(shell date -u +'"\"%Y-%m-%dT%H:%M:%SZ\""') \
-D__BUILDUSER__=$(shell printf '"\\"%s\\""' `id -nu`)

### Assembler ##########################################################
ASMOPTS	= --verbose-list -m
ASM	= 64tass

CHUNKS_ASM	= $(wildcard work/*.asm)
CHUNKS_LST	= $(CHUNKS_ASM:.asm=.lst)
chunks_prn	= $(CHUNKS_ASM:.asm=.prn)

# Where the org-tangle binary resides?  In this case we are using the
# development version.
#ORG_TANGLE=$(HOME)/st/src/org-tangle/src/org-tangle
#ORG_TANGLE=$(HOME)/st/repo/sc/src/org-tangle/src/org-tangle
ORG_TANGLE=org-tangle
ORG_WEAVE=org-weave

SOURCES	=
OBJECTS	= $(SOURCES:.c=.o)
EXECUTABLES =
orgs	= $(wildcard *.org)
pdfs	= $(orgs:.org=.pdf)
htmls	= $(orgs:.org=.html)
txts	= $(orgs:.org=.txt)
outputs = $(addprefix $(output)/pdf/,$(pdfs)) $(addprefix $(output)/txt/,$(txts))

# Folder for created pdf, html and texts.
output=$(HOME)/st/doc
VPATH = $(output)

# Other pandoc options to be considered:
#
PANDOC_OPTS= --tab-stop=8 # --top-level-division=chapter
PANDOC_PDF_OPTS	= $(PANDOC_OPTS) -V geometry:a4paper,margin=3cm
PANDOC_PDF_OPTS += --number-sections --toc --toc-depth=3
ifeq ($(SYSTEM), Darwin)
PANDOC_PDF_OPTS += --pdf-engine xelatex
else
PANDOC_PDF_OPTS += --highlight-style tango
endif
PANDOC_PDF_OPTS +=  $(COLOR_LINKS)

PANDOC_HTML_OPTS = $(PANDOC_OPTS) -t html -N
PANDOC_HTML_OPTS += -c styling.css
PANDOC_HTML_OPTS += -s
PANDOC_HTML_OPTS += --toc --toc-depth=3 --top-level-division=chapter

# Is not recognised in 2.2.1
# PANDOC_HTML_OPTS += --embed-resources
# As a workaround I copy the styling.css manually into output folder.

PANDOC_TXT_OPTS  = $(PANDOC_OPTS) -t plain
COLOR_LINKS = -V colorlinks=true -V linkcolor=blue -V urlcolor=red -V toccolor=gray

default: all

all:	$(SOURCES) $(EXECUTABLES) $(outputs) $(CHUNKS_LST) public/f256k-notes.html

clean:
	rm -v $(OBJECTS) $(EXECUTALBES) Makefile.deps *~ $(pdfs) $(txts)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

# Assembler
work/%.lst: work/%.a65
	-$(ASM) $(ASMOPTS) -l $@ $< 2>&1 | grep -v "^Pass"
	./cut-blocks.sh $@ chunks
	@echo expand $@ $* $(*F) $(%F)

work/%.lst: work/%.asm
	-$(ASM) $(ASMOPTS) -L $@ $<
	./cut-blocks.sh $@ chunks
	@echo expand $@ $* $(*F) $(%F)


-include Makefile.deps
Makefile.deps:
	$(CC) $(CFLAGS) -MM *.[c] > Makefile.deps

### Rule to weave the .org file to .worg file
%.worg: %.org
	$(ORG_TANGLE) $<
	$(ORG_WEAVE) -f -o $@ $<

### Pandoc Rules for worg files
# NOTICE that I supplied '--from=org' option as pandoc do not
# necesarily recognize '.worg' files.
#
$(output)/txt/%.txt: %.worg
	pandoc $(PANDOC_TXT_OPTS) --from=org -o $@ $<
	cp $@ public/

$(output)/pdf/%.pdf: %.worg $(CHUNKS_LST)
	pandoc $(PANDOC_PDF_OPTS) --from=org -o $@ $<
	cp $@ public/

public/%.html: %.worg
	pandoc $(PANDOC_HTML_OPTS) --from=org -o $@ $<
	cp styling.css public/

# ORG to TXT (UTF-8) transformation
# %.txt: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-ascii-export-to-ascii

# # ORG to PDF usign LaTeX export
# %.pdf: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-latex-export-to-pdf
# 	rm -v $(basename $@).tex

# # ORG to HTML
# %.html: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-html-export-to-html

#Eof:$Id: Makefile,v 0.3 2024-01-04 00:13:13+01 radek Exp radek $
