#!/bin/bash
# $Id$
# $Source$
# Get all blocks from listing and put them into separate files.

# Usage: listing directory
if [[ $# -ne 2 ]]
then
	cat <<-END_OF_USAGE
	usage: cut-blocks.sh lst-file directory
	Process lst-file and put all the blocks into directory in separate files.
	END_OF_USAGE
	exit 1
fi

LST=$1
BLOCKS=$(awk "/[[:space:]]*;MARK:/ {print \$2}" $LST)
OUTDIR=$2

[[ -d $OUTDIR ]] || mkdir -p $OUTDIR

echo -n "Cutting blocks:"
for block in $BLOCKS
do
	echo -n " $block"
	(echo "Listing of code chunk <<$block>>" ; \
	 awk "/[[:space:]]*;MARK: $block[[:space:]]*$/,/[[:space:]];ENDMARK/" $LST \
		 |tail -n +2 \
		 |head -n -1 \
		 |expand) >${OUTDIR}/${block}.prn
done
echo
