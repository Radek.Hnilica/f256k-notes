# Foenix F256K Notes

Radek private notes to F256K computer.

## License
This project, or its parts, are distributed with licenses:
- document files (org files) are licensed under CC BY-NC-SA-4.0
  license, unless stated otherwise.
- program source files are licensed under GPL v3.0 or newer.
- code snippets in documents are Public Domain, unless stated
  otherwise.

## Project status
This are living notes.  There is no plan or schedule to have them
finished.

## Publish
The generated notes are published as
[pdf](https://radek-hnilica.gitlab.io/f256k-notes/f256k-notes.pdf),
[html](https://radek-hnilica.gitlab.io/f256k-notes/f256k-notes.html),
and plain
[txt](https://radek-hnilica.gitlab.io/f256k-notes/f256k-notes.txt).
