#!/bin/bash
# $Id: mk.sh,v 0.0 $
# $Source: mk.sh,v $
# Build programs and documents.
#
# Copyright (c) 2023 Radek Hnilica
# All rights reserved.
# License: GPLv3, or ask me.

echo "Building F256K Notes"

declare -r FOLDERS=""

for folder in $FOLDERS; do
    echo "Processing subfolder $folder"
done

# Build here.
make 2>&1 | tee make.log

# Creating source code chunks from listing files.
# for lstfile in work/*.lst
# do
# 	: ./cut-blocks $lstfile chunks
# done
